#####################################################################
# 2_UPAM GSE25 (UP-AM Project GSE76225)                             #
#####################################################################

# Author: David Martínez-Enguita (2019)

# 2.1 Importation of MODifieR inputs
# 2.2 Module identification: MCODE
# 2.3 Module identification: Clique Sum
# 2.4 Module identification: DIAMOnD
# 2.5 Module identification: Module Discoverer
# 2.6 Module identification: Correlation Clique
# 2.7 Module identification: DiffCoEx
# 2.8 Module identification: MODA
# 2.9 Module identification: WGCNA
# 2.10 Generation and storage of MODifieR modules

# Args[1]: $input   - Path to Modifier input
# Args[2]: $network - Path to PPI network
# Args[3]: $results - Path to Results/Modules folder
# Args[4]: $name    - Dataset name
# Args[5]: $db      - Path to SQLite database
# Args[6]: $temp    - Path to temporary folder

Args <- commandArgs(TRUE)
library(MODifieR)

#####################################################################
# 2.1 Importation of MODifieR inputs                                #
#####################################################################

modifier_input <- readRDS(file = Args[1])
ppi_network <- read.csv(Args[2], stringsAsFactors = FALSE)

module_list <- list()

#####################################################################
# 2.2 Module identification: MCODE                                  #
#####################################################################

# MCODE
opt_haircut <- TRUE
opt_fluff <- c(TRUE, FALSE)
opt_vwp <- seq(0, 1, 0.1)
opt_fdt <- seq(0, 1, 0.1)

for (opt1 in opt_haircut) {
  for (opt2 in opt_fluff) {
    for (opt3 in opt_vwp) {
      for (opt4 in opt_fdt) {
        skip_to_next <- FALSE
        tryCatch(
          mcode_out <- mod_mcode(MODifieR_input = modifier_input, 
                                 ppi_network    = ppi_network, 
                                 hierarchy      = 1, 
                                 vwp            = opt3, 
                                 haircut        = opt1, 
                                 fluff          = opt2, 
                                 fdt            = opt4, 
                                 loops          = TRUE, 
                                 deg_cutoff     = 0.05, 
                                 module_cutoff  = 3.5, 
                                 dataset_name   = NULL), 
          error = function(e) { skip_to_next <<- TRUE })
        if (skip_to_next) { next }
        if (length(mcode_out[[1]]$module_genes) >= 1) {
          module_list[[length(module_list) + 1]] <- unique(c(mcode_out[[1]]$module_genes, mcode_out[[2]]$module_genes))
          names(module_list)[[length(module_list)]] <- paste0("mcode_hair_", opt1, "_fluff_", opt2, "_vwp_", 
                                                              opt3, "_fdt_", opt4, "_size_",
                                                              length(unique(c(mcode_out[[1]]$module_genes, mcode_out[[2]]$module_genes))))
        } else { module_list[[length(module_list) + 1]] <- NA }
      }
    }
  }
}

#####################################################################
# 2.3 Module identification: Clique Sum                             #
#####################################################################

# SQLite database construction
#build_clique_db(ppi_network = ppi_network, 
                #db_folder   = Args[5], 
                #db_name     = paste0("SQL_db_", Args[4]))

# Clique Sum
opt_cliquesignif <- seq(0.01, 0.1, 0.01)
opt_mincliquesize <- 4:8

for (opt1 in opt_cliquesignif) {
  for (opt2 in opt_mincliquesize) {
    skip_to_next <- FALSE
    tryCatch(
      cliquesum_out <- clique_sum_permutation(MODifieR_input      = modifier_input, 
                                              db                  = paste0(Args[5], "/SQL_db_", Args[4], ".sqlite"), 
                                              n_iterations        = 10000, 
                                              clique_significance = opt1, 
                                              min_clique_size     = opt2, 
                                              multiple_cores      = FALSE, 
                                              n_cores             = 4, 
                                              dataset_name        = NULL), 
      error = function(e) { skip_to_next <<- TRUE })
    if (skip_to_next) { next }
    if (length(cliquesum_out$module_genes) >= 1 ) {
      module_list[[length(module_list) + 1]] <- c(cliquesum_out$module_genes)
      names(module_list)[[length(module_list)]] <- paste0("cliquesum_cliquesignif_", opt1, "_minclique_", opt2, 
                                                          "_size_", length(cliquesum_out$module_genes))
    } else { 
      module_list[[length(module_list) + 1]] <- "NA"
      names(module_list)[[length(module_list)]] <- paste0("cliquesum_cliquesignif_", opt1, "_minclique_", opt2,
                                                          "_size_", length(cliquesum_out$module_genes))
    }
  }
}

#####################################################################
# 2.4 Module identification: DIAMOnD                                #
#####################################################################

# DIAMOnD
opt_degcutoff <- seq(0.01, 0.1, 0.01)
opt_seedweight <- seq(0, 14, 2)

for (opt1 in opt_degcutoff) {
  for (opt2 in opt_seedweight) {
    skip_to_next <- FALSE
    tryCatch(
      diamond_out <- diamond(MODifieR_input = modifier_input, 
                             ppi_network    = ppi_network, 
                             deg_cutoff     = opt1, 
                             n_output_genes = 200, 
                             seed_weight    = opt2, 
                             include_seed   = TRUE, 
                             dataset_name   = NULL), 
      error = function(e) { skip_to_next <<- TRUE })
    if (skip_to_next) { next }
    if (length(diamond_out$module_genes) >= 1 ) {
      module_list[[length(module_list) + 1]] <- c(diamond_out$module_genes)
      names(module_list)[[length(module_list)]] <- paste0("diamond_degcutoff_", opt1, "_seedweight_", opt2,
                                                          "_size_", length(diamond_out$module_genes))
    } else { 
      module_list[[length(module_list) + 1]] <- "NA" 
      names(module_list)[[length(module_list)]] <- paste0("diamond_degcutoff_", opt1, "_seedweight_", opt2,
                                                          "_size_", length(diamond_out$module_genes))
    }
  }
}

#####################################################################
# 2.5 Module identification: Module Discoverer                      #
#####################################################################

# Module Discoverer
skip_to_next <- FALSE
tryCatch(
  discoverer_out <- modulediscoverer(MODifieR_input = modifier_input, 
                                     ppi_network    = ppi_network, 
                                     permutations   = 1000, 
                                     deg_cutoff     = 0.05, 
                                     repeats        = 15, 
                                     clique_cutoff  = 0.01, 
                                     dataset_name   = NULL),
  error = function(e) { skip_to_next <<- TRUE })
if (skip_to_next) { next }
if (length(discoverer_out$module_genes) >= 1 ) {
  module_list[[length(module_list) + 1]] <- c(discoverer_out$module_genes)
  names(module_list)[[length(module_list)]] <- paste0("discoverer_size_", length(discoverer_out$module_genes))
} else { module_list[[length(module_list) + 1]] <- "NA" }

#####################################################################
# 2.6 Module identification: Correlation Clique                     #
#####################################################################

# Correlation Clique
skip_to_next <- FALSE
tryCatch(
  corclique_out <- correlation_clique(MODifieR_input           = modifier_input, 
                                      ppi_network              = ppi_network, 
                                      frequency_cutoff         = 0.1,
                                      fraction_of_interactions = 0.4,
                                      iteration                = 10, 
                                      clique_significance      = 0.01, 
                                      deg_cutoff               = 0.05, 
                                      multiple_cores           = FALSE, 
                                      n_cores                  = 4, 
                                      tempfolder               = Args[6], 
                                      dataset_name             = NULL), 
  error = function(e) { skip_to_next <<- TRUE })
if (skip_to_next) { next }
if (length(corclique_out$module_genes) >= 1 ) {
  module_list[[length(module_list) + 1]] <- c(corclique_out$module_genes)
  names(module_list)[[length(module_list)]] <- paste0("corclique_size_", length(corclique_out$module_genes))
} else { module_list[[length(module_list) + 1]] <- "NA" }

#####################################################################
# 2.7 Module identification: DiffCoEx                               #
#####################################################################

# DiffCoEx
opt_cutheight1 <- seq(0.95, 1, 0.005)
opt_cutheight2 <- seq(0, 0.3, 0.05)
opt_pval <- c(0.1, 0.05, 0.01, 0.001, 0.0001) 
opt_degcutoff <- c(0.1, 0.05, 0.01, 0.001, 0.0001) 

for (opt1 in opt_cutheight1) {
  for (opt2 in opt_cutheight2) {
    for (opt3 in opt_pval) {
      for (opt4 in opt_degcutoff) {
        skip_to_next <- FALSE
        tryCatch(
          diffcoex_out <- diffcoex(MODifieR_input    = modifier_input, 
                                   beta              = NULL, 
                                   cor_method        = "pearson", 
                                   cluster_method    = "average", 
                                   cuttree_method    = "hybrid", 
                                   cut_height        = opt1, 
                                   deepSplit         = 0, 
                                   pamRespectsDendro = FALSE, 
                                   minClusterSize    = 20,
                                   cutHeight         = opt2, 
                                   pval_cutoff       = opt3, 
                                   deg_cutoff        = opt4, 
                                   dataset_name      = NULL), 
          error = function(e) { skip_to_next <<- TRUE })
        if (skip_to_next) { next }
        if (length(diffcoex_out$module_genes) >= 1 ) {
          module_list[[length(module_list) + 1]] <- c(diffcoex_out$module_genes)
          names(module_list)[[length(module_list)]] <- paste0("diffcoex_cut1_", opt1, "_cut2_", opt2,
                                                              "_pval_", opt3, "_degcutoff_", opt4, 
                                                              "_size_", length(diffcoex_out$module_genes))
        } else { module_list[[length(module_list) + 1]] <- "NA" }
      }
    }
  }
}

#####################################################################
# 2.8 Module identification: MODA                                   #
#####################################################################

# MODA
opt_specific <- seq(0, 1, 0.1)
opt_conserved <- seq(0, 1, 0.1)

for (opt1 in opt_specific) {
  for (opt2 in opt_conserved) {
    skip_to_next <- FALSE
    tryCatch(
      moda_out <- moda(MODifieR_input    = modifier_input, 
                       cutmethod         = "Density", 
                       specificTheta     = opt1,
                       conservedTheta    = opt2, 
                       group_of_interest = 2, 
                       dataset_name      = NULL), 
      error = function(e) { skip_to_next <<- TRUE })
    if (skip_to_next) { next }
    if (length(moda_out$module_genes) >= 1 ) {
      module_list[[length(module_list) + 1]] <- c(moda_out$module_genes)
      names(module_list)[[length(module_list)]] <- paste0("moda_spec_", opt1, "_cons_", opt2, "_size_", 
                                                          length(moda_out$module_genes))
    } else { module_list[[length(module_list) + 1]] <- "NA" }
  }
}

#####################################################################
# 2.9 Module identification: WGCNA                                  #
#####################################################################

# WGCNA
opt_cutheight <- seq(0, 0.3, 0.05)
opt_pval <- c(0.0001, 0.001, 0.01, 0.05, 0.1) 

for (opt1 in opt_cutheight) {
  for (opt2 in opt_pval) {
    skip_to_next <- FALSE
    tryCatch(
      wgcna_out <- wgcna(MODifieR_input    = modifier_input,
                         minModuleSize     = 30, 
                         deepSplit         = 2, 
                         pamRespectsDendro = FALSE, 
                         mergeCutHeight    = opt1, 
                         numericLabels     = TRUE, 
                         pval_cutoff       = opt2, 
                         corType           = "bicor", 
                         maxBlockSize      = 5000, 
                         TOMType           = "signed", 
                         saveTOMs          = FALSE, 
                         dataset_name      = NULL), 
      error = function(e) { skip_to_next <<- TRUE })
    if (skip_to_next) { next }
    if (length(wgcna_out$module_genes) >= 1 ) {
      module_list[[length(module_list) + 1]] <- c(wgcna_out$module_genes)
      names(module_list)[[length(module_list)]] <- paste0("wgcna_cutheight_", opt1, "_pval_", opt2, "_size_", 
                                                          length(wgcna_out$module_genes))
    } else { module_list[[length(module_list) + 1]] <- "NA" }
  }
}

#####################################################################
# 2.10 Generation and storage of MODifieR modules                   #
#####################################################################

# Merge the module genes to create and store a PASCAL input object
module_length <- lapply(module_list, length)
module_longest <- which.max(module_length)
genes_longest <- unlist(module_list[module_longest])
module_bind <- as.data.frame(matrix(data = NA, ncol = length(genes_longest), nrow = 0))

for (i in 1:length(module_list)) {
  mod_row <- c(NA, module_list[[i]], rep(NA, length(genes_longest) - length(module_list[[i]])))
  module_bind <- rbind(module_bind, mod_row, stringsAsFactors = FALSE)
  if (is.null(names(module_list)[i])) {
    rownames(module_bind)[i] <- paste0("NULL", i)
  } else if (is.na(names(module_list)[i])) {
    rownames(module_bind)[i] <- paste0("NA", i)
  } else { rownames(module_bind)[i] <- names(module_list)[i] }
}

write.table(module_bind, file = paste0(Args[3], "allmodules_", Args[4], ".txt"), row.names = TRUE, 
            col.names = FALSE, quote = FALSE, sep = "\t", dec = ".")
