#####################################################################
# 1_UPAM GSE62 (UP-AM Project GSE76262)                             #
#####################################################################

# Author: David Martínez-Enguita (2019)

# 1.1 Importation of expression data
# 1.2 Quality control (AffyQC)
# 1.3 Creation of objects required by MODifieR input
# 1.4 Generation and storage of MODifieR input
# 1.5 Generation and storage of personalized MODifieR inputs

# Set library path to update and load required packages
lib_path <- .libPaths()[1]
update.packages(lib.loc = lib_path, ask = FALSE, checkBuilt = TRUE, dependencies = TRUE)
BiocManager::install(lib.loc = lib_path, update = TRUE, ask = FALSE, version = "3.9")
pack_R <- c("GEOquery", "data.table", "dplyr", "MODifieR", "stringr", 
            "akima", "viridis", "metap", "clusterProfiler", "ReactomePA", 
            "AnnotationHub", "DOSE", "xlsx", "rowr")
for (i in 1:length(pack_R)) {
  library(pack_R[i], character.only = TRUE)
}

# Set and build main directory structure
main_dir <- "/home/dme/Documents/UPAM_project/GSE62/"
new_dir <- c("RData", "Results", "R_scripts", "QC")
new_subdir <- c("Figures", "Modules", "RDS_files", "Tables")
for (dir in new_dir) {
  dir.create(path = paste0(main_dir, dir), showWarnings = FALSE) 
}
for (dir in new_subdir) {
  dir.create(paste0(main_dir, "Results/", dir), showWarnings = FALSE) 
}

rm(list = c("dir", "lib_path", "new_dir", "new_subdir"))

# Transfer QC files to main directory
set.affyqc.dir <- function() {
  list_files <-list.files("/home/dme/Documents/Standard_Directory/", full.names = TRUE) 
  file.copy(list_files, to = main_dir, overwrite = FALSE, recursive = TRUE,
            copy.mode = TRUE, copy.date = FALSE)
}
set.affyqc.dir()

#####################################################################
# 1.1 Importation of expression data                                #
#####################################################################

# ID dataset and platform
gset_name <- "GSE76262"
platform_name <- "GPL13158"

# Download dataset and platform data
gset <- GEOquery::getGEO(gset_name, destdir = paste0(main_dir, "DATA.DIR"), 
                         GSEMatrix = TRUE, AnnotGPL = FALSE)
GEOquery::getGEOSuppFiles(gset_name, makeDirectory = FALSE, 
                          baseDir = paste0(main_dir, "RAWDATA"))
gpl <- GEOquery::getGEO(platform_name, destdir = paste0(main_dir, "DATA.DIR"))

## Create probe annotation map
# Returns a corrected and robust 1:1:1 mapping of probes, Gene Symbols, and Entrez IDs
# Set max_maps to the maximum number of gene IDs stored in a single row of probe_map_raw
# Set id_division to the characters dividing different gene IDs in the same row (including spaces)
probe_map_raw <- GEOquery::Table(gpl)[c("ID", "Gene Symbol", "ENTREZ_GENE_ID")]

annotate.probes <- function(probe_map, max_maps, id_division) {
  probe_map_exp <- probe_map[!(probe_map$`Gene Symbol` == "" | is.na(probe_map$`Gene Symbol`) | probe_map$ENTREZ_GENE_ID == "" | is.na(probe_map$ENTREZ_GENE_ID)), ]
  colnames(probe_map_exp) <- gsub(pattern = "Gene Symbol", replacement = "Gene_Symbol", colnames(probe_map_exp))
  colnames(probe_map_exp) <- gsub(pattern = "ENTREZ_GENE_ID", replacement = "Entrez_ID", colnames(probe_map_exp))
  
  suppressWarnings(probe_map_exp <- setDT(probe_map_exp)[, paste0("Gene_Symbol", 1:max_maps) := tstrsplit(Gene_Symbol, id_division, type.convert = TRUE, fixed = TRUE, warnings(-1))])
  suppressWarnings(probe_map_exp <- setDT(probe_map_exp)[, paste0("Entrez_ID", 1:max_maps) := tstrsplit(Entrez_ID, id_division, type.convert = TRUE, fixed = TRUE, warnings(-1))])
  
  for (i in 1:nrow(probe_map_exp)) {
    probe_map_row <- probe_map_exp[i, ]
    for (j in 1:(max_maps - 1)) {
      if (is.na(probe_map_row[, ncol(probe_map) + 1 + j, with = FALSE]) == FALSE & 
          is.na(probe_map_row[, ncol(probe_map) + (max_maps + 1) + j, with = FALSE]) == TRUE) {
        probe_map_row[, ncol(probe_map) + (max_maps + 1) + j] <- probe_map_row[, ncol(probe_map) + max_maps + j, with = FALSE]
      } else if (is.na(probe_map_row[, ncol(probe_map) + 1 + j, with = FALSE]) == TRUE &
                 is.na(probe_map_row[, ncol(probe_map) + (max_maps + 1) + j, with = FALSE]) == TRUE) {
        break } } 
    probe_map_exp[i, ] <- probe_map_row
    {Sys.sleep(0.000001); cat("\r", paste("|-- INFO: Annotation corrected for row ", i, "/", nrow(probe_map_exp), 
                                          " (", sprintf("%.2f", (i/(nrow(probe_map_exp))*100)), " %) --|", sep = ""))}
  }
  for (k in 1:max_maps) {
    if (k == 1) {
      probe_map_temp <- probe_map_exp[, 1:ncol(probe_map)]
      probe_map_temp[, "Gene_Symbol"] <- probe_map_exp[, ncol(probe_map) + k, with = FALSE]
      probe_map_temp[, "Entrez_ID"] <- probe_map_exp[, ncol(probe_map) + max_maps + k, with = FALSE]
      probe_map_temp <- probe_map_temp[complete.cases(probe_map_temp$Gene_Symbol) & complete.cases(probe_map_temp$Entrez_ID), ]
    } else if (k > 1) {
      probe_map_add <- probe_map_exp[, 1:ncol(probe_map)]
      probe_map_add[, "Gene_Symbol"] <- probe_map_exp[, ncol(probe_map) + k, with = FALSE]
      probe_map_add[, "Entrez_ID"] <- probe_map_exp[, ncol(probe_map) + max_maps + k, with = FALSE]
      probe_map_add <- probe_map_add[complete.cases(probe_map_add$Gene_Symbol) & complete.cases(probe_map_add$Entrez_ID), ]
      probe_map_temp <- rbind(probe_map_temp, probe_map_add)
    }
  }
  probe_map_bind <- probe_map_temp %>% group_by (Entrez_ID)
  probe_map_bind[, ] <- lapply(probe_map_bind[, ], as.character)
  return(as.data.frame(probe_map_bind))
}

probe_map <- annotate.probes(probe_map = probe_map_raw, max_maps = 22, id_division = " /// ")

#####################################################################
# 1.2 Quality control (AffyQC)                                      #
#####################################################################

# Quality control with AffyAnalysisQC
apply.affyqc <- function() {
  # Extract CEL files
  untar(paste0(main_dir, "RAWDATA/", gset_name, "_RAW.tar"), exdir = paste0(main_dir, "RAWDATA/CEL"))
  cels <- list.files(path = paste0(main_dir, "RAWDATA/CEL"), pattern = "CEL.gz")
  # Quality control with AffyAnalysisQC
  pheno <- lapply(gset$GSE69683_series_matrix.txt.gz@phenoData@data, as.character)
  label <- as.factor(pheno$geo_accession)
  cohort <- as.factor(pheno$source_name_ch1)
  desc.df <- data.frame(Sample = cels, Label = label, Cohort = cohort)
  write.table(desc.df, paste0(main_dir, "description.txt"), quote = FALSE, sep = "\t", row.names = FALSE)
  source(file = paste0(main_dir, "affyAnalysisQC.R"))
}
apply.affyqc()
setwd(main_dir)

clean.affyqc <- function() {
  remove <- c("aType", "bgPlot", "boxplotNorm", "boxplotRaw", "CDFtype", "clusterNorm", "clusterOption1", 
              "clusterOption2", "clusterRaw", "col", "controlPlot", "correlNorm", "correlRaw", "customCDF", 
              "DATA.DIR", "degPlot", "densityNorm", "densityRaw", "DESC.DIR", "descfile", "experimentFactor", 
              "extension", "file_order", "HEIGHT", "hybrid", "layoutPlot", "legendColors", "legendSymbols",
              "MANorm", "MAOption1", "MARaw", "maxArray", "normFileName", "normMeth", "normOption1", "Nuse",
              "PCANorm", "PCARaw", "percPres", "PLMimage", "plotColors", "plotSymbols", "PMAcalls", 
              "POINTSIZE", "posnegCOI", "posnegDistrib", "quality", "ratio", "rawData.pset", "refName",
              "reOrder", "Rle", "samplePrep", "scaleFact", "SCRIPT.DIR", "spatialImage", "species", 
              "version_nb", "WIDTH", "WORK.DIR", "rawData", "normData", 
              "addStandardCDFenv", "addUpdatedCDFenv", "arrayGroup", "AffyQCExtension", "array.image", 
              "backgroundPlot", "backgroundPlotModif", "boxplotFun", "clusterFun", "colorsByFactor", 
              "computePMAtable", "controlPlots", "correctDIR", "correlFun", "coverAndKeyPlot", 
              "createNormDataTable", "deduceSpecies", "densityFun", "densityFunUnsmoothed", 
              "getArrayType", "hybridPlot", "hybridPlotModif", "maFun", "normalizeData", "nuseFun", 
              "pcaFun", "percPresPlot", "plotArrayLayout", "PNdistrPlot", "PNposPlot", "QCtablePlot", 
              "ratioPlot", "ratioPlotModif", "reload", "rleFun", "RNAdegPlot", "samplePrepPlot", 
              "samplePrepPlotModif", "scaleFactPlot", "spatialImages")
  rm(list = remove, pos = 1)
}
clean.affyqc()

# Load normalized data (RMA) and CEL file description 
expression_matrix <- read.delim(paste0(main_dir, "WORK.DIR/RMANormData_.txt"), row.names = 1, stringsAsFactors = FALSE)
description <- read.delim(paste0(main_dir, "description.txt"), stringsAsFactors = FALSE)

# QC directories, scripts, and output stored in /QC

#####################################################################
# 1.3 Creation of objects required by MODifieR input                #
#####################################################################

# Install MODifieR 0.1.4
devtools::install_git(url = "https://gitlab.com/Gustafsson-lab/MODifieR.git")

## Import PPI network
# Score: 0, 500, 600, 700, 800, 900
# Category: coexpression, combined_score, cooccurence, database, experimental, 
# fusion, neighborhood, textmining
network_score <- "900"
network_category <- "combined_score"
ppi_network <- read.csv(paste0(substr(main_dir, 1, 33), "PPI_network/", network_category, "/entrez_", 
                               network_category, "_", network_score, ".txt"), stringsAsFactors = FALSE)

# Create sample classifier and group indices
sample_classifier <- matrix(data = NA, nrow = nrow(description), ncol = 3)
colnames(sample_classifier) <- c("Sample", "Category", "Group")
sample_classifier[, 1] <- description$Label
for (i in 1:nrow(description)) {
  if (grepl(description$Cohort[i], pattern = "severe") == TRUE) {
    sample_classifier[i, 2] <- "SA"
    sample_classifier[i, 3] <- "A"
  } else if (grepl(description$Cohort[i], pattern = "moderate") == TRUE) {
    sample_classifier[i, 2] <- "MA"
    sample_classifier[i, 3] <- "A"
  } else if (grepl(description$Cohort[i], pattern = "healthy") == TRUE) {
    sample_classifier[i, 2] <- "C" 
    sample_classifier[i, 3] <- "C" 
  }
}

# Cross-check expression matrix and annotation table
annot_table <- probe_map[, c(1, 3)]
colnames(annot_table) <- c("PROBEID", "ENTREZID")
expression_matrix <- expression_matrix[rownames(expression_matrix) %in% annot_table$PROBEID, ]
annot_table <- annot_table[annot_table$PROBEID %in% rownames(expression_matrix), ]

# Store/Load expression matrix, annotation table, sample classifier
saveRDS(object = expression_matrix, file = paste0(main_dir, "Results/RDS_files/expression_matrix.RDS"))
saveRDS(object = annot_table, file = paste0(main_dir, "Results/RDS_files/annot_table.RDS"))
saveRDS(object = sample_classifier, file = paste0(main_dir, "Results/RDS_files/sample_classifier.RDS"))

expression_matrix <- readRDS(file = paste0(main_dir, "Results/RDS_files/expression_matrix.RDS"))
annot_table <- readRDS(file = paste0(main_dir, "Results/RDS_files/annot_table.RDS"))
sample_classifier <- readRDS(file = paste0(main_dir, "Results/RDS_files/sample_classifier.RDS"))

# Select groups of interest for comparison
group_index_1 <- as.numeric(which(sample_classifier[, 3] == "A"))
group_index_2 <- as.numeric(which(sample_classifier[, 3] == "C"))

# If needed, filter expression matrix to include only groups of interest
group_filter <- c(group_index_1, group_index_2)
aux_classifier <- sample_classifier[group_filter, ]
group_index_1 <- as.numeric(which(aux_classifier[, 2] == "SA"))
group_index_2 <- as.numeric(which(aux_classifier[, 2] == "C"))
expression_matrix <- expression_matrix[, group_filter]
rm(list = c("group_filter", "aux_classifier"))

#####################################################################
# 1.4 Generation and storage of MODifieR input                      #
#####################################################################

# Assign specific names to duplicated probes in annotation table
rename.duplicated.probes <- function(probe_id) {
  for(elt in probe_id) {
    pos <- which(probe_id == elt)
    occ <- length(pos)
    if (occ > 1) {
      probe_id[pos[occ]] <- paste0(probe_id[pos[occ]], ".", occ)
    }
  }
  return(probe_id)
}

annot_table$PROBEID <- rename.duplicated.probes(probe_id = annot_table$PROBEID)

# Expand expression matrix to accommodate for the duplicated probes with specific names
expand.expression.matrix <- function(probe_id, exp_mat) {
  dup <- probe_id[!(probe_id %in% rownames(exp_mat))]
  exp_mat[dup, ] <- exp_mat[sapply(dup, FUN = function(s) { return(strsplit(s, split="[.][1-9]*$")[[1]][1]) }), ]
  return(exp_mat)
}

expression_matrix <- expand.expression.matrix(probe_id = annot_table$PROBEID, exp_mat = expression_matrix)

# Create and store MODifieR input
modifier_input <- create_input(expression_matrix       = expression_matrix,
                               annotation_table        = annot_table,
                               group1_indici           = group_index_1,
                               group2_indici           = group_index_2,
                               group1_label            = "Asthma",
                               group2_label            = "Control",
                               expression              = TRUE,
                               differential_expression = TRUE, 
                               method                  = "MaxMean", 
                               filter_expression       = FALSE, 
                               use_adjusted            = FALSE)

saveRDS(object = modifier_input, file = paste0(main_dir, "Results/RDS_files/modifier_input.RDS"))
modifier_input <- readRDS(file = paste0(main_dir, "Results/RDS_files/modifier_input.RDS"))

#####################################################################
# 1.5 Generation and storage of personalized MODifieR inputs        #
#####################################################################

## Create and store personalized MODifieR input
# The expanded expression_matrix is subsetted to include 1 patient + all controls

for (i in 1:nrow(sample_classifier)) {
  # Time estimation [START]
  if (i == 1) {
    time_start = proc.time()[[3]]
  }  
  
  # Sample category (P: patient; C: control)
  if (i %in% group_index_1) {
    sample_cat <- "P"
  } else if (i %in% group_index_2) {
    sample_cat <- "C"
  }
  
  # Subset expression matrix (remove duplicated sample if needed)
  filt_mat <- expression_matrix[, c(i, group_index_2)]
  filt_mat <- filt_mat[, -(i + 1)]
  
  # Create personalized MODifieR input
  suppressMessages(
    modifier_personal <- create_input(expression_matrix       = filt_mat,
                                      annotation_table        = annot_table,
                                      group1_indici           = c(1),
                                      group2_indici           = c(2:ncol(filt_mat)),
                                      group1_label            = "Sample",
                                      group2_label            = "Control",
                                      expression              = TRUE,
                                      differential_expression = TRUE, 
                                      method                  = "MaxMean", 
                                      filter_expression       = FALSE, 
                                      use_adjusted            = FALSE)
  )
  
  # Store personalized MODifieR input
  saveRDS(object = modifier_personal, file = paste0(main_dir, "Results/RDS_files/Personal/", sample_cat, 
                                                    "_", colnames(filt_mat)[1], ".RDS"))
  
  # Time estimation [END]
  if (i == 1) {
    time_end = proc.time()[[3]]
    loop_time <- time_end - time_start
  }   
  time_left <- chron::times((loop_time*(nrow(sample_classifier)) - (loop_time*i))/(3600*24))
  
  {Sys.sleep(0.000001); cat("\r", paste0("|-- INFO: Personalized inputs created for ", i, "/", nrow(sample_classifier), 
                                         " samples (", sprintf("%.2f", (i/nrow(sample_classifier))*100)), " %, ", 
                            as.character(time_left), " remaining) --|", sep = "")}
}
