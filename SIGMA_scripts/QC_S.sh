#!/bin/bash

#SBATCH -J affyQC
#SBATCH -N 1
#SBATCH -t 1:00:00
#SBATCH -o ./Slurm_out/run_%j.out # STDOUT
#SBATCH -e ./Slurm_err/run_%j.err # STDERR

export OMP_NUM_THREADS=32
module load R/3.6.0-nsc1-gcc-7.3.0

R --no-save --no-restore CMD BATCH "--args $1 $2 $3 $4" QC_UPAM.R /home/x_damar/Documents/UPAM_project/Scripts/Rout_files/QC_UPAM.Rout
