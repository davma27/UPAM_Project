#!/bin/bash

mkdir -p Slurm_out Slurm_err

gset="GSE69683"
platform="GPL13158"
maindir="~/Documents/UPAM_project/GSE83/"

if [ $gset = "GSE69683" ]
then
cohort="characteristics_ch1"
else
cohort="source_name_ch1"
fi

sbatch QC_S.sh $gset $platform $maindir $cohort

