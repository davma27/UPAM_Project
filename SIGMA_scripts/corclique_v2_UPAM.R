#####################################################################
# corclique_UPAM (UP-AM Project)                                    #
#####################################################################

# Author: David Martínez-Enguita (2019)

# 2.1 Importation of MODifieR inputs
# 2.2 Module identification with MODifieR: Correlation Clique

# Args[1]: $input    - Path to Modifier input
# Args[2]: $network  - Path to PPI network
# Args[3]: $results  - Path to Results/Modules folder
# Args[4]: $name     - Dataset name
# Args[5]: $db       - Path to SQLite database
# Args[6]: $corrpval - Path to corrPvalues folder

Args <- commandArgs(TRUE)
library(MODifieR)
set.seed(777)

#####################################################################
# 2.1 Importation of MODifieR inputs                                #
#####################################################################

modifier_input <- readRDS(file = Args[1])
ppi_network <- read.csv(Args[2], stringsAsFactors = FALSE)

#####################################################################
# 2.2 Module identification with MODifieR: Correlation Clique       #
#####################################################################

module_list <- list()

## v2 function
correlation_clique_v2 <- function(MODifieR_input = modifier_input, 
                                  ppi_network = ppi_network, 
                                  frequency_cutoff = 0.5,
                                  fraction_of_interactions = 0.4, 
                                  iteration = 2, 
                                  clique_significance = 0.01, 
                                  deg_cutoff = 0.05, 
                                  multiple_cores = FALSE, 
                                  n_cores = NULL, 
                                  corrpval_path = NULL,
                                  min_clique_size = 2,
                                  dataset_name = NULL) {
  # F1
  evaluated_args <- c(as.list(environment()))
  settings <- as.list(stackoverflow::match.call.defaults()[-1])
  #replace_args <- names(settings)[!names(settings) %in% unevaluated_args]
  #for (argument in replace_args) {
  #settings[[which(names(settings) == argument)]] <- evaluated_args[[which(names(evaluated_args) == argument)]]
  #}
  # F2
  if (!is.null(dataset_name)) {
    settings$MODifieR_input <- dataset_name
  }
  # F3
  MODifieR_input$diff_genes <- MODifieR_input$diff_genes[MODifieR_input$diff_genes$gene %in% 
                                                           rownames(MODifieR_input$annotated_exprs_matrix), ]
  pValueMatrix <- stats::na.omit(MODifieR_input$diff_genes)
  unidir_network <- ppi_network[ppi_network[, 1] %in% pValueMatrix[, 1] &
                                  ppi_network[, 2] %in% pValueMatrix[, 1], 1:3]
  unidir_network[, 1:2] <- lapply(unidir_network[, 1:2], as.character)
  unidir_network <- unidir_network[!duplicated(data.frame(t(apply(unidir_network[1:2], 1, sort)), unidir_network[, 3])), ]
  unidir_network[, 3] <- unidir_network[, 3] / 1000
  # F4
  calculate_correlation <- function(row, expression_matrix) {
    row <- as.character(row)
    x_y <- c(which(rownames(expression_matrix) == row[1]), which(rownames(expression_matrix) == row[2]))
    1 - stats::cor.test(x = expression_matrix[x_y[1], ], y = expression_matrix[x_y[2], ])$p.value
  }
  if (file.exists(corrpval_path)) {
    corrPvalues <- readRDS(corrpval_path)
  } else if (!file.exists(corrpval_path)) {
    corrPvalues <- cbind(apply(X = unidir_network, MARGIN = 1, FUN = calculate_correlation, 
                               expression_matrix = MODifieR_input$annotated_exprs_matrix), 
                         unidir_network[, 3])
    saveRDS(corrPvalues, file = corrpval_path)
  }
  # F5
  pval_score <- apply(X = corrPvalues, MARGIN = 1, FUN = function(x) { sqrt(x[1] * x[2]) })
  edge_score <- pval_score * (length(pval_score) * fraction_of_interactions) / sum(pval_score)
  genes <- stats::setNames(pValueMatrix[, 2], pValueMatrix[, 1])
  genes <- genes[names(genes) %in% unique(unlist(unidir_network[, 1:2]))]
  # F6
  entrez_to_index <- function(entrez_id, name_table) {
    name_table[which(name_table[, 1] == entrez_id), 2]
  }
  
  index_to_entrez <- function(gene_index, name_table) {
    name_table[which(name_table[, 2] == gene_index), 1]
  }
  
  create_fisher_tables <- function(clique_size, total_n_genes, n_sig, unsig) {
    min_size <- min(c(clique_size, n_sig))
    fisher_table <- cbind(1:min_size, clique_size - 1:min_size, n_sig - 1:min_size, unsig - clique_size + 1:min_size)
    fisher_table[which(fisher_table < 0)] <- 0
    return(fisher_table)
  }
  
  evaluate_table <- function(fisher_table, clique_significance, min_deg_in_clique) {
    for (i in 1:nrow(fisher_table)) {
      if (MODifieR:::fisher_pval(clique_row = fisher_table[i, ]) <= clique_significance)
        return(max(i, min_deg_in_clique))
    }
    return (100000)
  }
  
  get_clique_cutoffs <- function(clique_size, deg_names, non_deg_genes, genes, clique_significance) {
    create_fisher_tables(clique_size = clique_size,
                         total_n_genes = length(genes),
                         n_sig = length(deg_names),
                         unsig = length(non_deg_genes)) %>%
      evaluate_table(fisher_table = .,
                     clique_significance = clique_significance,
                     min_deg_in_clique = 1)
  }
  
  check_significance <- function(clique, cutoff_per_size) {
    if (clique[2] >= cutoff_per_size[clique[1]]) {
      return(TRUE)
    } else { return(FALSE) }
  }
  
  create_clique_file <- function(adjacency_matrix, genes) {
    clique_file <- tempfile()
    g <- igraph::simplify(igraph::graph.data.frame(adjacency_matrix, directed = FALSE))
    genes <- genes[names(genes) %in% V(g)$name]
    name_table <- cbind(V(g)$name, as.character(as.numeric(V(g) - 1)))
    names(genes) <- sapply(X = names(genes), FUN = entrez_to_index, name_table = name_table)
    igraph::max_cliques(graph = g, file = clique_file, min = min_clique_size)
    return(list(tempfile = clique_file, graphed_ppi = g, name_table = name_table, tr_genes = genes))
  }
  
  process_clique <- function(clique_file, genes, graphed_ppi, clique_significance, deg_cutoff, name_table) {
    deg_genes <- genes[genes < deg_cutoff]
    non_deg_genes <- genes[genes >= deg_cutoff]
    cutoff_per_size <- vector(mode = "numeric")
    con <- file(clique_file, "r")
    significant_cliques <- NULL
    deg_names <- names(deg_genes)
    while (TRUE) {
      line <- readLines(con, n = 100000)
      if ( length(line) == 0 ) {
        break
      }
      clique_genes <- sapply(X = line, FUN = strsplit, split = " ")
      size_and_deg <- unname(sapply(X = clique_genes, FUN = function(x) c(length(x), sum(x %in% deg_names))))
      clique_sizes <- unique(size_and_deg[1, ])
      if (sum(is.na(cutoff_per_size[clique_sizes]) > 0)) {
        indici <- clique_sizes[which(is.na(cutoff_per_size[clique_sizes]))]
        cutoff_per_size[indici] <- sapply(X = indici, 
                                          FUN = get_clique_cutoffs, 
                                          deg_names = deg_names, 
                                          non_deg_genes = non_deg_genes,
                                          genes = genes,
                                          
                                          clique_significance = clique_significance)
      }
      significant_vector <- apply(X = size_and_deg, 
                                  MARGIN = 2, 
                                  FUN = check_significance, 
                                  cutoff_per_size = cutoff_per_size)
      significant_cliques <- unique(c(significant_cliques, unlist(clique_genes[significant_vector])))
    }
    significant_cliques <- unname(unique(unlist(sapply(X = significant_cliques, 
                                                       FUN = index_to_entrez, 
                                                       name_table = name_table))))
    close(con)
    file.remove(clique_file)
    return (unique(significant_cliques))
  }
  
  perform_iterations <- function(iteration_n, edge_score, unidir_network, genes, clique_significance, deg_cutoff) {
    to_pass <- edge_score > runif(n = length(edge_score))
    clique_file_list <- create_clique_file(adjacency_matrix = unidir_network[to_pass, ], genes = genes)
    corclique_list <- process_clique(clique_file = clique_file_list$tempfile, 
                                  genes = clique_file_list$tr_genes, 
                                  graphed_ppi = clique_file_list$graphed_ppi, 
                                  clique_significance = clique_significance,
                                  deg_cutoff = deg_cutoff, 
                                  name_table = clique_file_list$name_table)
  }
  
  construct_correlation_module <- function(module_genes, frequency_table, input_class, settings) {
    new_correlation_clique_module <- list("module_genes" = module_genes,
                                          "frequency_table" = frequency_table,
                                          "settings" = settings)
    class(new_correlation_clique_module) <- c("MODifieR_module", "Correlation_clique", input_class)
    return(new_correlation_clique_module)
  }
  
  corclique_list <- list()
  if (multiple_cores) {
    cl <- parallel::makeCluster(n_cores)
    doParallel::registerDoParallel(cl)
    parallel::clusterCall(cl, function(x) .libPaths(x), .libPaths())
    clusterEvalQ(cl, { library(MODifieR) })
    corclique_list <- parLapply(cl = cl, X = 1:iteration, fun = perform_iterations, edge_score, unidir_network, genes, clique_significance, deg_cutoff)
    parallel::stopCluster(cl)
  } else {
    for (i in 1:iteration) {
      corclique_list[[i]] <- perform_iterations(iteration_n = i, 
                                             edge_score = edge_score, 
                                             unidir_network = unidir_network, 
                                             genes = genes, 
                                             clique_significance = clique_significance, 
                                             deg_cutoff = deg_cutoff)
    }
  }
  tabled_frequencies <- table(unlist(corclique_list)) / iteration
  module_genes <- names(tabled_frequencies[tabled_frequencies >= frequency_cutoff])
  new_correlation_clique_module <- construct_correlation_module(module_genes = module_genes,
                                                                frequency_table = tabled_frequencies,
                                                                input_class = class(MODifieR_input)[3],
                                                                settings = settings)
  return(new_correlation_clique_module)
}

# Correlation Clique
opt_cliquesignif <- seq(0.01, 0.1, 0.01)
opt_degcutoff <- seq(0.01, 0.1, 0.01)

for (opt1 in opt_cliquesignif) {
  for (opt2 in opt_degcutoff) {
    skip_to_next <- FALSE
    tryCatch(
    corclique_out <- correlation_clique_v2(MODifieR_input           = modifier_input, 
                                           ppi_network              = ppi_network, 
                                           frequency_cutoff         = 0.5,
                                           fraction_of_interactions = 0.4,
                                           iteration                = 100, 
                                           clique_significance      = opt1, 
                                           deg_cutoff               = opt2, 
                                           multiple_cores           = TRUE, 
                                           n_cores                  = 32, 
                                           corrpval_path            = paste0(Args[6], "corrPvalues.RDS"),
                                           min_clique_size          = 2, 
                                           dataset_name             = NULL), 
    error = function(e) { skip_to_next <<- TRUE })
  if (skip_to_next) { next }
  if (length(corclique_out$module_genes) >= 1 ) {
    module_list[[length(module_list) + 1]] <- c(corclique_out$module_genes)
    names(module_list)[[length(module_list)]] <- paste0("corclique_cliquesignif_", opt1, "_degcutoff_", 
                                                        opt2, "_size_", length(corclique_out$module_genes))
    } else {
      module_list[[length(module_list) + 1]] <- "NA" 
      names(module_list)[[length(module_list)]] <- paste0("corclique_cliquesignif_", opt1, "_degcutoff_", 
                                                          opt2, "_size_", length(corclique_out$module_genes))
    }
  }
}

# Merge the module genes to create and store a PASCAL input object
module_length <- lapply(module_list, length)
module_longest <- which.max(module_length)
genes_longest <- unlist(module_list[module_longest])
module_bind <- as.data.frame(matrix(data = NA, ncol = length(genes_longest), nrow = 0))

for (j in 1:length(module_list)) {
  mod_row <- c(NA, module_list[[j]], rep(NA, length(genes_longest) - length(module_list[[j]])))
  module_bind <- rbind(module_bind, mod_row, stringsAsFactors = FALSE)
  if (is.null(names(module_list)[j])) {
    rownames(module_bind)[j] <- paste0("NULL", j)
  } else if (is.na(names(module_list)[j])) {
    rownames(module_bind)[j] <- paste0("NA", j)
  } else { rownames(module_bind)[j] <- names(module_list)[j] }
}

write.table(module_bind, file = paste0(Args[3], "corclique_allvar_", Args[4], ".txt"), row.names = TRUE, 
            col.names = FALSE, quote = FALSE, sep = "\t", dec = ".")
