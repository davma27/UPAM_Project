#!/bin/bash

mkdir -p Slurm_out Slurm_err

for name in GSE25 GSE26 GSE62 GSE83
do
input="~/Documents/UPAM_project/$name/Results/RDS_files/modifier_input.RDS"
network="~/Documents/UPAM_project/PPI_network/entrez_combined_score_900.txt"
results="~/Documents/UPAM_project/$name/Results/Modules/"
db="/home/x_damar/Documents/UPAM_project/Scripts/SQLite_database/"
corrpval="/home/x_damar/Documents/UPAM_project/$name/Results/RDS_files/"
echo $name
sbatch diamond_S.sh $input $network $results $name $db $corrpval
done
